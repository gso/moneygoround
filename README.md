# MoneyGoRound (alpha release)

MoneyGoRound is an application that takes as input details of cyclic transactions (e.g., salary on the last day of the month) projecting them forward in a cash flow analysis with running totals.

This is the code repository for the <http://moneygoround.info> website.  The repository also contains a class, [`MoneyGoRound`](https://bitbucket.org/gso/moneygoround/js/moneygoround/moneygoround.js), that implements the core functionality of the web site.

### Documentation
See [example.html](https://bitbucket.org/gso/moneygoround/src/master/example.html).

### Related Projects
[EonJS](https://bitbucket.org/gso/eonjs/src/master/), implements a recur rule container class.

### Contact
[support@moneygoround.info](mailto:support@moneygoround.info)

### License
GPL-3.0-or-later
