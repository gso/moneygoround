class MoneyGoRound {

	constructor(..._containerItemAry) {  // recur rule container item, rule or container
		this._recurRuleContainer = new RecurRuleContainer(..._containerItemAry);
	}

	set _recurRuleContainer(_recurRuleContainer) {
		if (!(_recurRuleContainer instanceof RecurRuleContainer)) throw new TypeError();
		this.recurRuleContainer_ = _recurRuleContainer;
	}

	get _recurRuleContainer() {
		return this.recurRuleContainer_;
	}

	add(..._recurRuleAry) {
		this._recurRuleContainer.add(..._recurRuleAry);
	}

	cashFlow(_startDate, _endDate, _openingBalance) {
		if (!(_startDate instanceof Date)) throw new TypeError();
		if (!(_endDate instanceof Date)) throw new TypeError();
		// #todo number check needs to include empty or spaces string or null or false
		if (_openingBalance === undefined || isNaN(_openingBalance)) throw new TypeError();
		if (_startDate > _endDate) throw new Error();

		const _transactionAry = this._initTransactionAry(_startDate, _endDate, _openingBalance);

		// Calc. running total
		let _runningTotal = 0;
		_transactionAry.forEach(function (_trx) {
			switch (_trx.transaction) {
				case "Income":
					_runningTotal = Number((_runningTotal + _trx.amount).toFixed(2));
					break;
				case "Outgoing":
					_runningTotal = Number((_runningTotal - _trx.amount).toFixed(2));
					break;
				default:
					throw new Error();
			}
			_trx.runningtotal = _runningTotal;
		});

		// Piggy bank and disposable income calculations
		let _outgoingsTotal = 0;  // accumulator for outgoing transactions
		for (let i = _transactionAry.length - 1; i >= 0; i--) {
			const _trx = _transactionAry[i];
			switch (_trx.transaction) {
				case "Income":
					if (_outgoingsTotal <= _trx.runningtotal) {
						// enough in the kitty (aka running total) to cover the outgoings total
						_trx.piggybank = _outgoingsTotal;
						// reset outgoings total
						_outgoingsTotal = 0;
						// calc. disposable income
						_trx.disposableincome = _trx.runningtotal - _trx.piggybank;
					} else if (_outgoingsTotal > _trx.runningtotal) {
						// not enough in the kitty (aka running total) to cover outgoings
						if (_trx.runningtotal >= 0) {
							// we can only put what we have in the kitty (aka running total) into the piggy bank
							_trx.piggybank = _trx.runningtotal;
						} else if (_trx.runningtotal < 0) {
							// running an overdraft, nothing in the piggy bank
							_trx.piggybank = 0;
						} else {
							throw new Error();
						}
						// no disposable income
						_trx.disposableincome = 0;
					} else {
						throw new Error();
					}
					break;
				case "Outgoing":
					if (_outgoingsTotal <= _trx.runningtotal) {
						// enough in the kitty (aka running total) to cover the outgoings total
						_trx.piggybank = _outgoingsTotal;
					} else if (_outgoingsTotal > _trx.runningtotal) {
						// not enough in the kitty (aka running total) to cover outgoings
						if (_trx.runningtotal >= 0) {
							// max. piggy bank is running total amount
							_trx.piggybank = _trx.runningtotal
						} else if (_trx.runningtotal < 0) {
							// running an overdraft, nothing in the piggy bank
							_trx.piggybank = 0;
						} else {
							throw new Error();
						}
					} else {
						throw new Error();
					}
					_outgoingsTotal = Number((_outgoingsTotal + _trx.amount).toFixed(2));
					_trx.disposableincome = "";
					break;
				default:
					throw new Error();
			}
		}

		return _transactionAry;
	}

	savingsCalc(_startDate, _reconcileDate, _openingBalance, _savingsAccountTotal) {
		if (!(_startDate instanceof Date)) throw new TypeError();
		if (!(_reconcileDate instanceof Date)) throw new TypeError();
		if (_openingBalance === undefined || isNaN(_openingBalance)) throw new TypeError();
		if (_savingsAccountTotal === undefined || isNaN(_savingsAccountTotal)) throw new TypeError();
		if (_savingsAccountTotal < 0) throw new Error();
		if (_startDate > _reconcileDate) throw new Error();

		let _idx;

		// Work out which income transaction(s) to make a transfer to savings directly following
		// get income recur rules
		const _recurRuleItr = this._recurRuleContainer.recurRule();
		const _incomeAry = [];
		let _next = _recurRuleItr.next();
		while (!_next.done) {
			const _recurRule = _next.value;
			if (_recurRule.data.transaction === "Income") {
					_incomeAry.push(_recurRule);
			}
			_next = _recurRuleItr.next();
		}
		// reverse sort on amount
		_incomeAry.sort(function (a, b) {
			// if a > b leave a where it is, return -1
			if (a.data.amount > b.data.amount) { return -1; }
			// if a < b, move b to a, return 1
			if (a.data.amount < b.data.amount) { return 1; }
			// if a == b return 0
			if (a.data.amount === b.data.amount) { return 0; }
		});
		// add savings transfer flag to recur rule data of income trx(s) with highest trx amount
		_idx = 0;
		while (_incomeAry[_idx].recurRule.toText() === "every day for 1 time") {
			// skip any single one off transactions
			_incomeAry[_idx].data.savingsTransferFlag = false;
			_idx += 1;
		}
		const _amount = _incomeAry[_idx].data.amount;
		while (_incomeAry[_idx].data.amount === _amount) {
			// flag transactions with the highest trx amount
			_incomeAry[_idx].data.savingsTransferFlag = true;
			_idx += 1;
		}
		const _incomeAryLen = _incomeAry.length;
		while (_idx < _incomeAryLen) {
			// flag the rest as false
			_incomeAry[_idx].data.savingsTransferFlag = false;
			_idx += 1;
		}

		const _transactionAry = this._initTransactionAry(_startDate, _reconcileDate, _openingBalance);

		// Calculate piggy bank totals between income transactions
		let _piggybankTotal = 0;
		for (let i = _transactionAry.length - 1; i >= 0; i--) {
			const _trx = _transactionAry[i];
			// output to piggy bank total
			_trx.piggybank = _piggybankTotal;
			switch (_trx.transaction) {
				case "Income":
					// zero piggy bank total
					_piggybankTotal = 0;
					break;
				case "Outgoing":
					// add outgoing amount to piggy bank total
					// _piggybankTotal += _trx.amount;
					_piggybankTotal = Number((_piggybankTotal + _trx.amount).toFixed(2));
					break;
				default:
					throw new Error();
			}
		}

		let _runningTotal = 0, _savingsTotal = _savingsAccountTotal;
		_idx = 0;
		while (_idx < _transactionAry.length) {  // array is spliced into, length will increase

			const _trx = _transactionAry[_idx];

			// Update running total
			switch (_trx.transaction) {
				case "Income":
					// _runningTotal += _trx.amount;
					_runningTotal = Number((_runningTotal + _trx.amount).toFixed(2));
					break;
				case "Outgoing":
					// _runningTotal -= _trx.amount;
					_runningTotal = Number((_runningTotal - _trx.amount).toFixed(2));
					break;
				default:
					throw new Error();
			}
			_trx.runningtotal = _runningTotal;

			// Insert savings transactions before income transaction(s) flagged for savings transfer
			switch (_trx.transaction) {
				case "Income":
					if (_trx.savingsTransferFlag) {
						const _prevRunningTotal = Number((_trx.runningtotal - _trx.amount).toFixed(2));
						// the savings transfer date is 1 day before the income transaction
						let _prevDayDate = new Date(_trx.date.getTime());
						_prevDayDate.setDate(_prevDayDate.getDate() - 1);
						if (_prevRunningTotal > 0) {
							const _savings = _prevRunningTotal;
							_savingsTotal = Number((_savingsTotal + _savings).toFixed(2));
							_transactionAry.splice(_idx, 0, {
								date: _prevDayDate,
								transaction: "Outgoing",
								description: "Transfer to savings account (MGR)",
								amount: _savings,
								runningtotal: 0,
								piggybank: 0,
								savingstotal: _savingsTotal
							});
							_runningTotal = _trx.amount;
							_trx.runningtotal = _runningTotal;
						} else if (_prevRunningTotal <= 0) {
							// zero or -ve running total, no savings to transfer
							_transactionAry.splice(_idx, 0, {
								date: _prevDayDate,
								transaction: "Outgoing",
								description: "Transfer to savings account (MGR)",
								amount: 0,
								runningtotal: _prevRunningTotal,
								piggybank: 0,
								savingstotal: _savingsTotal
							});
						} else {
							throw new Error();
						}
						_idx += 1;
					}
					// Piggy bank cannot be more than running total and is 0 if overdrawn
					if (_trx.piggybank > _trx.runningtotal) {
						if (_trx.runningtotal >= 0) {
							_trx.piggybank = _trx.runningtotal;
						} else if (_trx.runningtotal < 0) {
							_trx.piggybank = 0;
						} else {
							throw new Error();
						}
					}
					_trx.disposableincome = Number((_trx.runningtotal - _trx.piggybank).toFixed(2));
					delete _trx.savingsTransferFlag;
					break;
				case "Outgoing":
					if (_trx.runningtotal < 0 && _savingsTotal > 0) {
						// -ve running total (overdraft), transfer savings across
						const _prevRunningTotal = Number((_runningTotal + _trx.amount).toFixed(2));
						const _amountNeeded =
							Number((_trx.amount + _trx.piggybank - _prevRunningTotal).toFixed(2));
						const _savingsTransferred = _savingsTotal >= _amountNeeded ? _amountNeeded : _savingsTotal;
						const _newRunningTotal =
							Number((_prevRunningTotal + _savingsTransferred).toFixed(2));
						const _newSavingsTotal = Number((_savingsTotal - _savingsTransferred).toFixed(2));
						_transactionAry.splice(_idx, 0, {
							date: _trx.date,
							transaction: "Income",
							description: "Transfer from savings account (MGR)",
							amount: _savingsTransferred,
							runningtotal: _newRunningTotal,
							piggybank: Number((_trx.piggybank + _trx.amount).toFixed(2)),
							savingstotal: _newSavingsTotal
						});
						_trx.runningtotal =
							Number((_transactionAry[_idx].runningtotal - _trx.amount).toFixed(2));
						_idx += 1;
						_runningTotal = _trx.runningtotal;
						_savingsTotal = _newSavingsTotal;
					} else if (_trx.runningtotal >= 0 || _savingsTotal === 0) {
						// everything is OK (+ve balance) or no savings to transfer to -ve balance
					} else {
						throw new Error();
					}
					// Piggy bank cannot be more than running total and is zero if overdrawn
					if (_trx.piggybank > _trx.runningtotal) {
						if (_trx.runningtotal >= 0) {
							_trx.piggybank = _trx.runningtotal;
						} else if (_trx.runningtotal < 0) {
							_trx.piggybank = 0;
						} else {
							throw new Error();
						}
					}
					break;
				default:
					throw new Error();
			}
			_idx += 1;
		}

		// #todo Calc. disposable savings

		// Opening savings account balance (if greater than zero)
		if (_savingsAccountTotal) {
			_transactionAry.splice(0, 0, {
				date: _startDate,
				transaction: "Outgoing",
				description: "Savings account total",
				amount: _savingsAccountTotal,
				runningtotal: 0,
				piggybank: 0,
				savingstotal: _savingsAccountTotal
			});
		}

		return _transactionAry;
	}

	_initTransactionAry(_startDate, _endDate, _openingBalance) {

		const _recurrenceItr = this._recurRuleContainer.recurrence(_startDate, _endDate);
		const _transactionAry = [];
		let _next = _recurrenceItr.next();
		while (!_next.done) {
			const _occurence = _next.value;
			const _newTrx = {};
			_newTrx.date = _occurence.date;
			// transaction, description, amount, any other data
			Object.assign(_newTrx, _occurence.recurRule.data);
			_transactionAry.push(_newTrx);
			_next = _recurrenceItr.next();
		}

		// sort on amount ascending
		_transactionAry.sort((_trx1, _trx2) => _trx1.amount - _trx2.amount);
		// sort on transaction type ascending (inc., out.)
		_transactionAry.sort(function (_trx1, _trx2) {
			if (_trx1.transaction < _trx2.transaction) {
				return -1;
			}
			if (_trx1.transaction > _trx2.transaction) {
				return 1;
			}
			return 0;
		});
		// sort on date ascending
		_transactionAry.sort((_trx1, _trx2) => _trx1.date - _trx2.date);

		// unshift opening balance onto start of _transactionAry
		_transactionAry.unshift({
			date: _startDate,
			transaction: _openingBalance >= 0 ? "Income" : "Outgoing",
			description: "Opening balance",
			amount: Math.abs(_openingBalance)
		});

		return _transactionAry;
	}

}
